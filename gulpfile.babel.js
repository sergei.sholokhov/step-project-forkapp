import pkg from 'gulp';
import * as dartSass from 'sass';
import gulpSass from 'gulp-sass';
import fileInclude from 'gulp-file-include';
import browserSync from 'browser-sync';
// import uglify from 'gulp-uglify';
import terser from 'gulp-terser'; //minimize for ES6+
import concat from 'gulp-concat';
import cssnano from 'cssnano';
import rename from 'gulp-rename';
// import plumber from 'gulp-plumber'; 
// import purgecss from 'gulp-purgecss'; // purgecss outputs the source CSS with unused selectors removed
import gulpPostcss from 'gulp-postcss'
import flexFix from 'postcss-flexbugs-fixes'; 
import autoprefixer from 'autoprefixer'; 
import imagemin from 'gulp-imagemin';
import { deleteAsync } from 'del';

const sass = gulpSass(dartSass);
const { task, src, dest, watch, series, parallel } = pkg;

const cleanDist = () => {
	return deleteAsync(["./dist"]);
};

const moveJS = () => {
	return src("./src/js/*.js")
		.pipe(concat('script.js'))
		// .pipe(uglify()) 
		.pipe(terser()) 
		.pipe(rename({ suffix: '.min' }))
		.pipe(dest("./dist/scripts"))
		.pipe(browserSync.stream());;
}

const moveCSS = () => {
	const processors = [
		// auto fix some flex-box issues
		flexFix(),
		// auto adds vendor prefixes
		autoprefixer({
			grid: true,
			cascade: true
		})
	];

	return src("./src/scss/style.scss")
		.pipe(sass.sync({
			sourceComments: false,
			outputStyle: "expanded"
		})).on('error', sass.logError)
		// .pipe(purgecss({                     // bug with synthax &_name
            // content: ['src/**/*.scss']
        // }))
		.pipe(gulpPostcss(processors)) 
		.pipe(gulpPostcss([cssnano]))  
		.pipe(rename({ suffix: ".min" })) 
		.pipe(dest("./dist/css/"))
		.pipe(browserSync.stream());
}

const moveIMG = () => {
	return src("./src/img/**/*.+(jpg|svg|png)")
		.pipe(imagemin())
		.pipe(dest("./dist/img/"))
		.pipe(browserSync.stream());
}

const moveHTML = () => {
	return src("./src/*.html")
		.pipe(fileInclude())
		.pipe(dest("./"))
		.pipe(browserSync.stream());
}

const server = () => {
	return browserSync.init({
		server: {
			baseDir: ['./']
		},
		port: 9000,
		open: true
	})
}

task('clean', cleanDist);
task('moveJS', moveJS);
task('styleCSS', moveCSS);
task('moveHTML', moveHTML);
task('moveIMG', moveIMG);
task('serve', server);
task('watchers', () => {
	watch('./src/scss/**/*.scss', parallel('styleCSS')).on('change', browserSync.reload);
	watch('./src/views/*.html', parallel('moveHTML')).on('change', browserSync.reload);
	watch('./src/scripts/*.js', parallel('moveJS')).on('change', browserSync.reload);
});

task('built', series('clean', 'moveJS', 'styleCSS', 'moveIMG', 'moveHTML')); 
task('dev', series('built', parallel('serve', 'watchers')))

