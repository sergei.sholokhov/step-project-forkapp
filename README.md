# Step-project-ForkApp

Step Project "Forkio"

Node.js, Git, Sass, Gulp, Java Script, CSS, HTML

Student 1 - Serhiy Sholokhov
Tasks: 
1 - create project and initialize Git repository and main branch
2 - tasks gulp built, gulp dev
3 - layouts for the sections Header, Section_01 aka "Hero" and Section_04 "Testimonials". 

Student 2 - Ivan Repetskyi
Tasks:
1 - layouts for Section_02 "Revolutionary Editor", Section_03 "Here is what you get" and Section_05 "Fork Subscription Pricing".
